# Pulse

Python implementation of the pulseAPI JSON-RPC. Implementation details and spec can be found [here](https://momentfactory.atlassian.net/wiki/spaces/BIBLETECHNO/pages/2952364044/Pulse+1.0).