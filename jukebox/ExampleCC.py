from Tasktoolkit import*
from queue import Queue
import json
import time

controlCenterHostOrIp = '127.0.0.1'

# Load the tasktoolkit dll. Must be done once.
# The provider is an interface to all defined methods, they are divided into members to help the autocomplete.
toolkitProvider = ToolkitProvider()

global asyncTaskJobsQueue
asyncTaskJobsQueue = Queue()

def RefreshTaskCatalog():
    print("Pushing Catalog")
    toolkitProvider.publish.BeginCatalog(0)
    
    toolkitProvider.publish.CreateTaskGroup(b"My Group", b"My Group")
    
    toolkitProvider.publish.CreateActionTask(b"my_task_uid", b"My Task", b"This is a test task", 0)
    toolkitProvider.publish.SetTaskIcon(b"tag_faces")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_bool_uid", b"My Bool Param", b"My Bool Description", b"Bool", True)
    # must use true and not True
    toolkitProvider.publish.SetParameterDefaultValue(b"true")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_float_uid", b"My Float Paramzz", b"My Float Description", b"Float", True)
    # FIXME: when using 12.8 here, it will appear as 12.800000190734863 in the control center
    toolkitProvider.publish.SetParameterDefaultValue(b"12.8")

    # FIXME: if the parameter type string is not recognized, if will not log an error
    toolkitProvider.publish.CreateParameterFromString(b"my_task_integer_uid", b"My Integer Paramzz", b"My Integer Description", b"Int", True)
    toolkitProvider.publish.SetParameterDefaultValue(b"4")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_string_uid", b"My String Param", b"My String Description", b"String", True)
    toolkitProvider.publish.SetParameterDefaultValue(b"Hello World")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_color_uid", b"My Color Param", b"My Color Description", b"Color", True)
    toolkitProvider.publish.SetParameterDefaultValue(b"#fa1e4eff")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_stringlist_uid", b"My StringList Param", b"My StringList Description", b"StringList", True)
    toolkitProvider.publish.SetParameterCandidates(b"item1,item2,item3,item4")
    toolkitProvider.publish.SetParameterDefaultValue(b"item2")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_checklist_uid", b"My Checklist Param", b"My Checklist Description", b"Checklist", True)
    toolkitProvider.publish.SetParameterCandidates(b"item1,item2,item3,item4")
    # FIXME: seems the default value are not read properly by the CC when displaying the checklist
    toolkitProvider.publish.SetParameterDefaultValue(b"item1,item4")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_variablelist_uid", b"My VariableList Param", b"My Variable List Description", b"VariableList", True)
    toolkitProvider.publish.SetParameterCandidatesSourceVariableInternalName(b"my_task_parameter_source_variable1")

    toolkitProvider.publish.CreateParameterFromString(b"my_task_variablemultiselectlist_uid", b"My VariableMultiSelectList Param", b"My Variable Multiple Select List Description", b"VariableMultiSelectList", True)
    toolkitProvider.publish.SetParameterCandidatesSourceVariableInternalName(b"my_task_parameter_source_variable2")

    toolkitProvider.publish.EndCatalog(True)

def RefreshTaskVariables():
    toolkitProvider.publish.BeginVariable(b"my_task_parameter_source_variable1",  CCTaskToolkitVariableDataType.Dictionnary, False)
    # FIXME: allow passing a string in the value and not just an integer
    # FIXME: if 0 is used as a value, it will not be sent when selected in the CC, so it should not be used...
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic list 1", 1)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic list 2", 2)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic list 3", 3)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic list 4", 4)
    toolkitProvider.publish.EndDatastoreVariable(True)

    toolkitProvider.publish.BeginVariable(b"my_task_parameter_source_variable2",  CCTaskToolkitVariableDataType.Dictionnary, False)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic checklist 1", 1)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic checklist 2", 2)
    toolkitProvider.publish.AddDictionnaryMapping(b"dynamic checklist 3", 3)
    toolkitProvider.publish.EndDatastoreVariable(True)




def OnTaskExecutionRequest(taskName, taskNameStrLength, jsontaskParams, jsonTaskParamStrLengh, taskexecutionUid):
    prettyjson = json.dumps(json.loads(jsontaskParams), indent=4)
    print("request for executing task", taskName, " with parameters \n", prettyjson, " with execution id ", taskexecutionUid)

    # play with those parameters to toggle sync or async task execution, and success or error feedback to the control center
    inplaceExecution = True
    success = True

    feedbackMsg = b"Everything is perfect" if success else b"An error happened"
    feedbackJson = b'{"details" : "Some Details", "key" : "value" }'
    toolkitProvider.logic.SetTaskExecutionFeedback(feedbackMsg, len(feedbackMsg),  feedbackJson, len(feedbackJson))
    result = 0

    # return the appropriate value whether successfull or not
    if inplaceExecution:
        if success:
            result = int(CCTaskToolkitTaskExecutionResult.Success)
        else:
            result = int(CCTaskToolkitTaskExecutionResult.Error)
    else:
        #  keep track of task execution info for later by storing that into an array for later execution
        jobTask = {"taskName": taskName, "taskParams": jsontaskParams, "taskExecutionId": taskexecutionUid}
        asyncTaskJobsQueue.put(jobTask)
        result = int(CCTaskToolkitTaskExecutionResult.DifferAnswer)

    return result

def AsyncExecuteTask(taskName, jsontaskParams, taskExecutionId):
    feedbackMsg = b"Async Execution successfull"
    feedbackJson = b"\{\}"
    toolkitProvider.logic.NotifyAsyncTaskExecutionResult(taskExecutionId, CCTaskToolkitTaskExecutionResult.Success, feedbackMsg, len(feedbackMsg),  feedbackJson, len(feedbackJson))

def OnHandshakeEstablished(ccApiVersion):
    print("CC version : ", ccApiVersion)
    RefreshTaskVariables()
    RefreshTaskCatalog()

# To get the version of the tasktoolkit as string
print("Loading Tasktoolkit version : ", toolkitProvider.utility.GetVersionString().decode("utf-8"))

# Create callback from defined delegate we expose inside toolkitProvider.callback.
# You must keep those callback alive until the end or they would be garbage collected.
logCallback = toolkitProvider.callback.CCTaskToolkitLogCallbackDelegate(lambda timestamp, moduleName, level, versus, function, line, thId, callstack, msg : print("[", timestamp, "][", moduleName, "]: ", msg))

onCCConnectCallback = toolkitProvider.callback.CCTaskToolkitMQTTConnectionChangedDelegate(lambda connected: print("CC connection : ", connected))
onCCConnectEstablishedCallback = toolkitProvider.callback.CCTaskToolkitControlCenterConnectionEstablishedCallback(OnHandshakeEstablished)

onRefreshCallback = toolkitProvider.callback.CCTaskToolkitRefreshTaskCallback(RefreshTaskCatalog)
onTaskExecutionRequestCallback = toolkitProvider.callback.CCTaskToolkitTaskExecuteReceivedCallback(OnTaskExecutionRequest)

# Install the toolkit API.
# Specify what you want to install from the API with CCTaskToolkitInitParameters flags and | operator.
toolkitProvider.logic.installAPI(CCTaskToolkitInitParameters.CallbackLogger)

# Pre-initialisation

# Setting the callbacks. The log callback should come first or logs would be ignored until it is set.
toolkitProvider.callback.setAPILoggerCallback(logCallback)
toolkitProvider.callback.setMQTTConnectionChangedCallback(onCCConnectCallback)
toolkitProvider.callback.setControlCenterConnectionEstablishedCallback(onCCConnectEstablishedCallback)
toolkitProvider.callback.setRefreshTaskCallback(onRefreshCallback)
toolkitProvider.callback.setTaskExecuteReceivedCallback(onTaskExecutionRequestCallback)

# for some reason the display name is turning into lower cap when displayed in the CC
toolkitProvider.config.SetCatalogName(b"My Catalog Name")
toolkitProvider.config.SetDatastoreHost(controlCenterHostOrIp.encode('utf-8'))

toolkitProvider.config.SetSoftwareName(b"Python_Example_App", b"Python Example App")

# are we sure this is necessary ? I dont think so
# however if not called the catalog wont be published, but there is no warning log about it
toolkitProvider.config.SetLocalIP(b"127.0.0.1")

# Once pre-initialization was done, call initializeCCTaskToolkitAPI to really start the API.
# Beware: after this point, you won't be able to call any methods that needs to be called at pre-init time.
toolkitProvider.logic.initializeAPI()


# The loop called every frame.
totalFrame = 0
while totalFrame < 1000:
    time.sleep(0.033)
    # Call that to process any messages, ... the Tasktoolkit API needs. It should be called regularly. 
    toolkitProvider.logic.APIUpdate()

    if not asyncTaskJobsQueue.empty():
        jobTask = asyncTaskJobsQueue.get()
        AsyncExecuteTask(jobTask["taskName"], jobTask["taskParams"], jobTask["taskExecutionId"])
    
    totalFrame += 1

# Once the application exit
# Don't forget to cleanUp the API. That will uninit and shutdown the API. If you need to call the API again, you need to undergo the install + pre-init + init from scratch.
toolkitProvider.logic.cleanUpAPI()
