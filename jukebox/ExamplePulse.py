from Tasktoolkit import*
import time

controlCenterHostOrIp = '127.0.0.1'

# Load the tasktoolkit dll. Must be done once.
# The provider is an interface to all defined methods, they are divided into members to help the autocomplete.
toolkitProvider = ToolkitProvider()

# To get the version of the tasktoolkit as string
print("Loading Tasktoolkit version : ", toolkitProvider.utility.GetVersionString().decode("utf-8"))


# Create callback from defined delegate we expose inside toolkitProvider.callback.
# You must keep those callback alive until the end or they would be garbage collected.
logCallback = toolkitProvider.callback.CCTaskToolkitLogCallbackDelegate(lambda timestamp, moduleName, level, versus, function, line, thId, callstack, msg : print("[", timestamp, "][", moduleName, "]: ", msg))

onPulseConnectCallback = toolkitProvider.callback.CCTaskToolkitPulseOnConnectedCallback(lambda connected: print("Pulse connection : ", connected))

# Install the toolkit API.
# Specify what you want to install from the API with CCTaskToolkitInitParameters flags and | operator.
toolkitProvider.logic.installAPI(CCTaskToolkitInitParameters.WithPulse | CCTaskToolkitInitParameters.NoShowControl | CCTaskToolkitInitParameters.CallbackLogger)

# Pre-initialisation

# Don't forget to set the callbacks. The log callback should come first or logs would be ignored until it is set.
toolkitProvider.callback.setAPILoggerCallback(logCallback)
toolkitProvider.callback.PulseSetOnConnectedCallback(onPulseConnectCallback)

toolkitProvider.config.SetSoftwareName(b"PythonTestForTD", b"PythonTestForTD")

toolkitProvider.pulse.SetupPulseID(b"TST", b"zone", b"td", b"0")

# That method can be called anytime.
toolkitProvider.pulse.SetupConnectionSettings(controlCenterHostOrIp.encode('utf-8'), 0)

# Once pre-initialization was done, call initializeCCTaskToolkitAPI to really start the API.
# Beware: after this point, you won't be able to call any methods that needs to be called at pre-init time.
toolkitProvider.logic.initializeAPI()


state1Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('state1'.encode('utf-8'), PulseMetricsCategory.State, -1)
state2Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('state2'.encode('utf-8'), PulseMetricsCategory.State, -1)
role1Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('role1'.encode('utf-8'), PulseMetricsCategory.Role, -1)
role2Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('role2'.encode('utf-8'), PulseMetricsCategory.Role, -1)
metrics1Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('metrics1'.encode('utf-8'), PulseMetricsCategory.Metrics, -1)
metrics2Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('metrics2'.encode('utf-8'), PulseMetricsCategory.Metrics, -1)
subsystem1Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('subsystem1'.encode('utf-8'), PulseMetricsCategory.Subsystem, -1)
subsystem2Pusher = toolkitProvider.pulse.AddNewCustomMetricsPusher('subsystem2'.encode('utf-8'), PulseMetricsCategory.Subsystem, -1)

totalFrame = 0

# The loop called every frame.
while totalFrame < 1000:
    time.sleep(0.033)
    # For pulse, you need to notify the new frame before the update
    toolkitProvider.pulse.NotifyNewFrame(30.0, 0)
    # Call that to process any messages, ... the Tasktoolkit API needs. It should be called regularly. 
    toolkitProvider.logic.APIUpdate()
    
    randUpdate = totalFrame % 8
    if randUpdate == 0:
        toolkitProvider.pulse.ExternalMetricsSetBoolValue(state1Pusher, 'rand bool'.encode('utf-8'), totalFrame % 3 == 0)
        toolkitProvider.pulse.ExternalMetricsSetIntValue(state1Pusher, 'rand int'.encode('utf-8'), totalFrame % 18)
    elif randUpdate == 1:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(state2Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)
    elif randUpdate == 2:
        toolkitProvider.pulse.ExternalMetricsSetStringValue(role1Pusher, 'rand string'.encode('utf-8'), ('*' * (totalFrame % 10)).encode('utf-8'))
    elif randUpdate == 3:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(role2Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)
    elif randUpdate == 4:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(metrics1Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)
    elif randUpdate == 5:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(metrics2Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)
    elif randUpdate == 6:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(subsystem1Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)
    elif randUpdate == 7:
        toolkitProvider.pulse.ExternalMetricsSetUIntValue(subsystem2Pusher, 'rand uint'.encode('utf-8'), totalFrame % 24 == 0)

    totalFrame += 1
    


# Once the application exit

toolkitProvider.pulse.RemoveExternalMetrics(state1Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(state2Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(role1Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(role2Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(metrics1Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(metrics2Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(subsystem1Pusher)
toolkitProvider.pulse.RemoveExternalMetrics(subsystem2Pusher)

# That's for Pulse to send the exit cause
toolkitProvider.pulse.NotifyExitCause(b"Exit App", True)


# Don't forget to cleanUp the API. That will uninit and shutdown the API. If you need to call the API again, you need to undergo the install + pre-init + init from scratch.
toolkitProvider.logic.cleanUpAPI()
