from ctypes import*
from enum import IntEnum
import platform
import os
import json


# CC
class CCTaskToolkitInitParameters(IntEnum):
	Minimal = 0
	CallbackLogger = 0X1 << 0
	NoShowControl = 0X1 << 1
	WithPulse = 0X1 << 2
	SingleThreadedCallbacks = 0X1 << 3
	CalibrationModule = 0X1 << 4
	UseCleanupCounter = 0X1 << 5

# I don't use IntEnum('CCTaskToolkitLogLevel', ['Debug', 'Logic', 'Info', 'Warning', 'Error', 'Fatal'], start = 0) because it won't generate autocomplete for the enum.
class CCTaskToolkitLogLevel(IntEnum):
	Debug = 0
	Logic = 1
	Info = 2
	Warning = 3
	Error = 4
	Fatal = 5

class CCTaskToolkitLogVersus(IntEnum):
	Cout = 0
	Local = 1
	Designer = 2
	Server = 3

class CCTaskToolkitTaskResultType(IntEnum):
	String = 0

class CCTaskToolkitTaskExecutionResult(IntEnum):
	Success = 0
	Warning = 1
	Error = 2
	DifferAnswer = 3

class CCTaskToolkitTaskDescriptorType(IntEnum):
	Method = 0

class CCTaskToolkitParameterDataType(IntEnum):
	Bool = 0
	Float = 1
	Integer = 2
	String = 3
	StringList = 4
	VariableMultiSelectList = 5
	VariableList = 6
	CheckList = 7
	Color = 8

class PulseVersionCategory(IntEnum):
	Runtime = 0
	Component = 1
	# To help decide on what category is MofaLib
	MofaLib = Component

class PulseMetricsCategory(IntEnum):
	Role = 0
	State = 1
	Subsystem = 2
	Metrics = 3

class PulseDataType(IntEnum):
	Double = 0
	Int64 = 1
	UInt64 = 2
	String = 3
	Bool = 4
	Object = 5
	Table = 6
	StringView = 7
	RawJSON = 8

class CCTaskToolkitVariableDataType(IntEnum):
	Integer = 0
	Float = 1
	Bool = 2
	Enum = 3
	JSON = 4
	String = 5
	Dictionnary = 6

# Pulse
class PulseConnectionMode(IntEnum):
	MQTT = 0

# Calibration
class CalibrationTechnique(IntEnum):
	Trilateration = 0
	SphereIntersect = 1
	HelmertTransform = 2
	Simple = 3

class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            # Create the instance and pass constructor arguments
            instance = super(SingletonMeta, cls).__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

# Provider Classes
class ToolkitProvider(metaclass=SingletonMeta):
	def __init__(self, dllPath = ''):
		if(len(dllPath) == 0):
			currentSystem = platform.system()
			if(currentSystem == "Windows"):
				currentDir = os.path.dirname(os.path.abspath(__file__))
				cdll.LoadLibrary(os.path.join(currentDir, "libcrypto-3-x64.dll"))
				cdll.LoadLibrary(os.path.join(currentDir, "libssl-3-x64.dll"))
				cdll.LoadLibrary(os.path.join(currentDir, "paho-mqtt3as.dll"))
				cdll.LoadLibrary(os.path.join(currentDir, "xagora-singleton.dll"))
				self.modules = cdll.LoadLibrary(os.path.join(currentDir, "cc-tasktoolkit.dll"))
			elif(currentSystem == "Linux"):
				distribution = platform.freedesktop_os_release()['ID']
				if(distribution == "ubuntu" or distribution == "debian"):
					distribution = "Ubuntu"
				elif(distribution == "alpine"):
					distribution = "Alpine"
				else:
					distribution = platform.freedesktop_os_release()['NAME']
					if(distribution.__contains__("Ubuntu") or distribution.__contains__("Debian")):
						distribution = "Ubuntu"
					elif(distribution.__contains__("Alpine")):
						distribution = "Alpine"
					else:
						raise Exception("Unsupported machine distribution")

				currentArchitecture = platform.machine()
				if(currentArchitecture == "arm"):
					currentArchitecture = "ARM"
				elif(currentArchitecture == "arm64" or currentArchitecture.startswith("aarch64")):
					currentArchitecture = "ARM64"
				elif(currentArchitecture == "x86_64"):
					currentArchitecture = "x64"
				else:
					raise Exception("Unsupported machine architecture")

				self.modules = cdll.LoadLibrary(os.path.join(distribution, currentArchitecture, "libcc-tasktoolkit.so"))
			else:
				raise Exception("Unsupported system.")
		else:
			self.modules = cdll.LoadLibrary(dllPath)

		self.logic = ToolkitProvider.Logic(self)
		self.callback = ToolkitProvider.Delegate(self)
		self.config = ToolkitProvider.Config(self)
		self.publish = ToolkitProvider.Publish(self)
		self.utility = ToolkitProvider.Utility(self)
		self.pulse = ToolkitProvider.Pulse(self)
		self.calibration = ToolkitProvider.Calibration(self)


	def makeFunc(self, modulesFunc, restype, argtypes):
		modulesFunc.restype = restype
		modulesFunc.argtypes = argtypes
		return modulesFunc

	class Logic:
		def __init__(self, parent):
			self.installAPI = parent.makeFunc(parent.modules.installCCTaskToolkitAPI, c_char_p, [c_int])
			self.initializeAPI = parent.makeFunc(parent.modules.initializeCCTaskToolkitAPI, c_bool, [])
			self.cleanUpAPI = parent.makeFunc(parent.modules.cleanUpCCTaskToolkitAPI, c_bool, [])
			self.APIUpdate = parent.makeFunc(parent.modules.CCTaskToolkitAPIUpdate, c_bool, [])
			self.apiInitialized = False

			# CC
			self.SetTaskExecutionFeedback = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskExecutionFeedback, c_bool, [c_char_p, c_uint64, c_char_p, c_uint64])
			self.NotifyAsyncTaskExecutionResult = parent.makeFunc(parent.modules.CCTaskToolkitNotifyAsyncTaskExecutionResult, c_bool, [c_uint64, c_int32, c_char_p, c_uint64, c_char_p, c_uint64])

		def initializeJukeboxAPI(self):
			self.initializeAPI()
			self.apiInitialized = True

		def isApiInitialized(self):
			return self.apiInitialized


	class Delegate:
		def __init__(self, parent):
			# Log callback method to pass to setAPILoggerCallback. This won't work (return false) if the task toolkit was not initialized with CCToolkitInitParameters::CallbackLogger
			self.CCTaskToolkitLogCallbackDelegate = CFUNCTYPE(None, c_char_p, c_char_p, c_int, c_int, c_char_p, c_int, c_char_p, c_char_p, c_char_p)

			# Callback for when a connection to the Control Center changed its state.
			# Note that this is just a mqtt client connection/disconnection event that fired but no telling the connection is genuine.
			# #To know the connection was really made, then wait for the ControlCenterConnectionEstablishedDelegate callbacks
			self.CCTaskToolkitMQTTConnectionChangedDelegate = CFUNCTYPE(None, c_bool)

			# CC functions
			# Set the callback for when a connection to the Control Center was established as genuine.
			# The argument received is the version of the Control Center.
			self.CCTaskToolkitControlCenterConnectionEstablishedCallback = CFUNCTYPE(None, c_char_p)
			# Set the callback for an unknown, therefore unhandled, mqtt message came and the toolkit doesn't know what to do with it.
			self.CCTaskToolkitAdditionalMQTTMessageReceivedQueueCallback = CFUNCTYPE(None, c_char_p, c_char_p, c_char_p, c_uint64, c_char_p, c_uint64)
			# Set the query delegate for the CCTaskToolkit API to pull the local IP of the station running the client code, from the client code.
			self.CCTaskToolkitOnRequestLocalIpQuery = CFUNCTYPE(c_uint64, c_char_p, c_uint64)
			# Set the callback for when a refresh request happened. It is expected the client code refresh its Catalog using Catalog Publishing methods (or Publisher) and publishes the constructed catalog
			self.CCTaskToolkitRefreshTaskCallback = CFUNCTYPE(None)
			# Set the callback called when the CCTaskToolkit API received a task execution request from CC.
			#															   taskName,   taskNamestrLength,  jsonTaskParams, jsonTaskParamStrLength, taskExecutionUid)
			self.CCTaskToolkitTaskExecuteReceivedCallback = CFUNCTYPE(c_int, c_char_p, c_uint64, c_char_p, c_uint64, c_uint64)
			# Set the callback called when the CCTaskToolkit API has updated the project topic (MF project code)
			self.CCTaskToolkitOnMqttProjectTopicChangedCallback = CFUNCTYPE(None, c_char_p, c_char_p, c_bool)
			# Set the callback for when a refresh request for the variables used in the published catalog happened
			self.CCTaskToolkitOnVariablesUpdateRefreshDelegate = CFUNCTYPE(None)
			# Set the callback for when the control center configuration is received upon connection
			self.CCTaskToolkitControlCenterConfigReceivedCallback = CFUNCTYPE(None, c_char_p, c_char_p, c_char_p, c_char_p)
			# Callback fired after Jukebox Plugin commands were processed, but before processing received mqtt messages.
			self.CCTaskToolkitOnPostCommandsProcessDelegate = CFUNCTYPE(None)

			self.setAPILoggerCallback = parent.makeFunc(parent.modules.setCCTaskToolkitAPILoggerCallback, c_bool, [self.CCTaskToolkitLogCallbackDelegate])
			self.setMQTTConnectionChangedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitMQTTConnectionChangedCallback, c_bool, [self.CCTaskToolkitMQTTConnectionChangedDelegate])
			self.setControlCenterConnectionEstablishedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitControlCenterConnectionEstablishedCallback, c_bool, [self.CCTaskToolkitControlCenterConnectionEstablishedCallback])
			self.setAdditionalMQTTMessageReceivedQueueCallback = parent.makeFunc(parent.modules.setCCTaskToolkitAdditionalMQTTMessageReceivedQueueCallback, c_bool, [self.CCTaskToolkitAdditionalMQTTMessageReceivedQueueCallback])
			self.setRefreshTaskCallback = parent.makeFunc(parent.modules.setCCTaskToolkitRefreshTaskCallback, c_bool, [self.CCTaskToolkitRefreshTaskCallback])
			self.setOnRequestLocalIpQuery = parent.makeFunc(parent.modules.setCCTaskToolkitOnRequestLocalIpQuery, c_bool, [self.CCTaskToolkitOnRequestLocalIpQuery])
			self.setTaskExecuteReceivedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitTaskExecuteReceivedCallback, c_bool, [self.CCTaskToolkitTaskExecuteReceivedCallback])
			self.setOnMqttProjectTopicChangedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnMqttProjectTopicChangedCallback, c_bool, [self.CCTaskToolkitOnMqttProjectTopicChangedCallback])
			self.setOnVariablesUpdateRefreshCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnVariablesUpdateRefreshCallback, c_bool, [self.CCTaskToolkitOnVariablesUpdateRefreshDelegate])
			self.setControlCenterConfigReceivedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitControlCenterConfigReceivedCallback, c_bool, [self.CCTaskToolkitControlCenterConfigReceivedCallback])
			self.setOnPostCommandsProcessCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnPostCommandsProcessCallback, c_bool, [self.CCTaskToolkitOnPostCommandsProcessDelegate])

			# Pulse: callback called when the connection to the mqtt broker has been established
			self.CCTaskToolkitPulseOnConnectedCallback = CFUNCTYPE(None, c_bool)
			# Pulse: callback called when the pulse:startup notification has been sent, returns the sent payload
			self.CCTaskToolkitPulseOnPostStartupCallback = CFUNCTYPE(None, c_char_p)
			# Pulse: callback called when the pulse:heartbeat notification has been sent, returns the sent payload
			self.CCTaskToolkitPulseOnPostHeartbeatCallback = CFUNCTYPE(None, c_char_p)

			self.PulseSetOnConnectedCallback = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetOnConnectedCallback, c_bool, [self.CCTaskToolkitPulseOnConnectedCallback])
			self.setPulseOnPostStartupCallback = parent.makeFunc(parent.modules.setCCTaskToolkitPulseOnPostStartupCallback, c_bool, [self.CCTaskToolkitPulseOnPostStartupCallback])
			self.setPulseOnPostHeartbeatCallback = parent.makeFunc(parent.modules.setCCTaskToolkitPulseOnPostHeartbeatCallback, c_bool, [self.CCTaskToolkitPulseOnPostHeartbeatCallback])

			# Calibration
			# Fired when Calibration Engine started processing the calibration request.
			self.CCTaskToolkitOnCalibrationStartedCallback = CFUNCTYPE(None)
			# Fired with the calibration result expressing the transformation (x,y,z translation, qx, qy, qz, qw quaternion, and px, py, pz the rotation pivot position) to realign calibrated device frame to source reference device frame (can also be used to express any point from one frame to the other).
			self.CCTaskToolkitOnCalibrationResultCallback = CFUNCTYPE(None, c_uint32, c_uint32, c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double)
			# Fired when the Calibration Engine ended processing the calibration request.
			# This callback is fired in all time at the end of the calibration process, whether the result.
			self.CCTaskToolkitOnCalibrationEndedCallback = CFUNCTYPE(None, c_bool, c_uint64, c_char_p)

			self.setCCTaskToolkitOnCalibrationStartedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnCalibrationStartedCallback, c_bool, [self.CCTaskToolkitOnCalibrationStartedCallback])
			self.setOnCalibrationResultCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnCalibrationResultCallback, c_bool, [self.CCTaskToolkitOnCalibrationResultCallback])
			self.setOnCalibrationEndedCallback = parent.makeFunc(parent.modules.setCCTaskToolkitOnCalibrationEndedCallback, c_bool, [self.CCTaskToolkitOnCalibrationEndedCallback])

		# set the callback to get notified when a given task needs to be executed
		def setCCTaskToolkitTaskExecuteReceivedCallback(self, newCallback):
			global taskExecuteReceivedCallback
			newtaskExecuteReceivedCallback = self.CCTaskToolkitTaskExecuteReceivedCallback(newCallback)
			self.setTaskExecuteReceivedCallback(newtaskExecuteReceivedCallback)
			taskExecuteReceivedCallback = newtaskExecuteReceivedCallback

		# set the callback to get notified when control center requests the catalog to be refreshed
		def setCCTaskToolkitRefreshTaskCallback(self, newCallback):
			global toolkitRefreshCallback
			newtoolkitRefreshCallback = self.CCTaskToolkitRefreshTaskCallback(newCallback)
			self.setRefreshTaskCallback(newtoolkitRefreshCallback)
			toolkitRefreshCallback = newtoolkitRefreshCallback

		def setCCTaskToolkitMQTTConnectionChanged(self, newCallback):
			global CCMqttConCallback
			newCCMqttConCallback = self.CCTaskToolkitMQTTConnectionChangedDelegate(newCallback)
			self.setMQTTConnectionChangedCallback(newCCMqttConCallback)
			CCMqttConCallback = newCCMqttConCallback

		# update the cc connection callback to use your own
		def setCCConnectEstablishedCallbackCallback(self, newCallback):
			global CCConCallback
			newCCConLogCb = self.CCTaskToolkitControlCenterConnectionEstablishedCallback(newCallback)
			self.setControlCenterConnectionEstablishedCallback(newCCConLogCb)
			CCConCallback = newCCConLogCb

		# update the log callback to use your own
		def setLogCallback(self, newCallback):
			global logCallback
			newLogCb = self.CCTaskToolkitLogCallbackDelegate(newCallback)
			self.setAPILoggerCallback(newLogCb)
			logCallback = newLogCb

		# set the heartbeat payload sent callback
		def setHeartBeatPayloadSentCallback(self, newCallback):
			global hbCallback
			newHbCallback = self.CCTaskToolkitPulseOnPostHeartbeatCallback(newCallback)
			self.setPulseOnPostHeartbeatCallback(newHbCallback)
			hbCallback = newHbCallback

		# set the startup payload sent callback
		def setStartupPayloadSentCallback(self, newCallback):
			global startupCallback
			newStartupCallback	= self.CCTaskToolkitPulseOnPostStartupCallback(newCallback)
			self.setPulseOnPostStartupCallback(newStartupCallback)
			startupCallback = newStartupCallback

	# feedback sent on pulse connection/disconnection from mqtt broker...
		def setOnPulseConnectCallback(self, newCallback):
			global onPulseConnectCallback
			newOnPulseConnectCb = self.CCTaskToolkitPulseOnConnectedCallback(newCallback)
			self.PulseSetOnConnectedCallback(newOnPulseConnectCb)
			onPulseConnectCallback =  newOnPulseConnectCb

	class Config:
		def __init__(self, parent):
			# set the hostname/ip of the control center to connect to (remove dns suffix, it should be the output of the hostname() command)
			# This call is enough when connecting to a CC 4.17 or greater.
			self.SetDatastoreHost = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreHost, c_bool, [c_char_p])
			# set the IP:port of the mqtt broker to connect to, either IP or IP:port, default port is 1883.
			# Note this call is ignored when connecting to a CC 4.17 or greater because it sends the configuration file with this settings.
			self.SetMqttBrokerHostAndPort = parent.makeFunc(parent.modules.CCTaskToolkitSetMqttBrokerHost, c_bool, [c_char_p])
			# set the name of the catalog to publish
			self.SetCatalogName = parent.makeFunc(parent.modules.CCTaskToolkitSetCatalogName, c_bool, [c_char_p])
			# set the project identifier of the catalog (usually the 3 letter MF project code)
			# Note this call is ignored when connecting to a CC 4.17 or greater because it sends the configuration file with this settings.
			self.SetProjectIdentifier = parent.makeFunc(parent.modules.CCTaskToolkitSetProjectIdentifier, c_bool, [c_char_p])
			# identifies the name of the software publishing the catalog, usually the tech used (touchdesigner, unity, unreal, etc)
			self.SetSoftwareName = parent.makeFunc(parent.modules.CCTaskToolkitSetSoftwareName, c_bool, [c_char_p, c_char_p])
			# Allows to set the local IP of the calling station. It can be called from within CCTaskToolkitOnRequestLocalIpQuery instead of filling the buffer, or can be called pre
			self.SetLocalIP = parent.makeFunc(parent.modules.CCTaskToolkitSetLocalIP, c_bool, [c_char_p])
			# Temporary mute all CC related stuff (tasks/callbacks/...) without breaking connection.
			self.SetIgnoreAllCCMessages = parent.makeFunc(parent.modules.CCTaskToolkitSetIgnoreAllCCMessages, c_bool, [c_bool])
			# display the callstack when an error log is emitted by the toolkit (default is false. Enable it at your own convenience for troubleshooting and error reporting, but keep in mind that callstack logging is resource expensive)
			self.SetShouldLogCallstack = parent.makeFunc(parent.modules.CCTaskToolkitSetShouldLogCallstack, c_bool, [c_bool])
			# when printing json data in log, prettify the json in a nicely formatted way (default is false)
			self.EnablePrettyJsonMode = parent.makeFunc(parent.modules.CCTaskToolkitEnablePrettyJsonMode, c_bool, [c_bool])
			# Set if we should print the task execution received from the CC. Only manually called task execution would be printed. Automatic status requests would be ignored.
			self.SetShouldLogManualTaskExecution = parent.makeFunc(parent.modules.CCTaskToolkitSetShouldLogManualTaskExecution, c_bool, [c_bool])
			# enable extra log verbosity (default is false)
			self.EnableEnableExtraLogVerbosity = parent.makeFunc(parent.modules.CCTaskToolkitEnableEnableExtraLogVerbosity, c_bool, [c_bool])

	# Catalog publishing functions
	class Publish:
		def __init__(self, parent):
			# Starts a catalog definition, should be called either in
			# CCTaskToolkitControlCenterConnectionEstablishedCallback (first handshake with CC)
			# setRefreshTaskCallback (when CC asks the catalog to be refreshed)
			# Begins the catalog definition, parameter is an estimation of the number of tasks it will contain for optimization purposes but it can also be 0, 													  estimated number of tasks
			self.BeginCatalog = parent.makeFunc(parent.modules.CCTaskToolkitBeginCatalog, c_bool, [c_uint64])
			# Ends the catalog definition, must be called before PublishCatalog					publish to CC right away
			self.EndCatalog = parent.makeFunc(parent.modules.CCTaskToolkitEndCatalog, c_bool, [c_bool])
			# Creates a variable
			self.CreateVariable = parent.makeFunc(parent.modules.CCTaskToolkitCreateVariable, c_bool, [c_char_p])
			# Set the mqtt topic to listen to be notified of the value changes of this variable
			self.SetVariablePath = parent.makeFunc(parent.modules.CCTaskToolkitSetVariablePath, c_bool, [c_char_p])
			# Only applies to numeric variables, enables the telemetry for this variable, visible on the graphana ui ( http://ip.of.control.center:3000 )
			self.SetVariableKeepHistory = parent.makeFunc(parent.modules.CCTaskToolkitSetVariableKeepHistory, c_bool, [c_bool])
			# Add a tag to the currently declared variable, can be called multiple times
			self.AddVariableTag = parent.makeFunc(parent.modules.CCTaskToolkitAddVariableTag, c_bool, [c_char_p])
			# TODO
			self.CreateStatusTaskPollingRule = parent.makeFunc(parent.modules.CCTaskToolkitCreateStatusTaskPollingRule, c_bool, [c_char_p])
			# TODO
			self.SetStatusTaskPollingRuleRunOnce = parent.makeFunc(parent.modules.CCTaskToolkitSetStatusTaskPollingRuleRunOnce, c_bool, [c_bool])
			# TODO
			self.SetStatusTaskPollingRuleRunOnceBeforeInterval = parent.makeFunc(parent.modules.CCTaskToolkitSetStatusTaskPollingRuleRunOnceBeforeInterval, c_bool, [c_bool])
			# TODO
			self.SetStatusTaskPollingRuleEnabled = parent.makeFunc(parent.modules.CCTaskToolkitSetStatusTaskPollingRuleEnabled, c_bool, [c_bool])
			# TODO
			self.SetStatusTaskPollingRuleIntervalMs = parent.makeFunc(parent.modules.CCTaskToolkitSetStatusTaskPollingRuleIntervalMs, c_bool, [c_int32])
			# Must be called before declaring a new task, defines the category name for all subsequently declared tasks
			self.CreateTaskGroup = parent.makeFunc(parent.modules.CCTaskToolkitCreateTaskGroup, c_bool, [c_char_p, c_char_p])
			# Creates a task to be executed on request
			self.CreateActionTask = parent.makeFunc(parent.modules.CCTaskToolkitCreateActionTask, c_bool, [c_char_p, c_char_p, c_char_p, c_int32])
			# Creates a task to be executed periodically
			self.CreateStatusTask = parent.makeFunc(parent.modules.CCTaskToolkitCreateStatusTask, c_bool, [c_char_p, c_char_p, c_char_p, c_int32])
			# TODO
			self.SetTaskDescriptorType = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskDescriptorType, c_bool, [c_int32])
			# TODO: Sets the type of the result returned by the tasktoolkit, only string is supported		resultType
			self.SetTaskResultType = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskResultType, c_bool, [c_int32])
			# Defines whether the task should confirm its execution to be considered successfully executed (default is false)
			self.SetTaskReplyRequired = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskReplyRequired, c_bool, [c_bool])
			# TODO
			self.SetTaskIsGeneric = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskIsGeneric, c_bool, [c_bool])
			# TODO
			self.SetTaskIcon = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskIcon, c_bool, [c_char_p])
			# defines an icon associated to the task, for the list of supported string check https://www.angularjswiki.com/angular/angular-material-icons-list-mat-icon-list/
			self.SetTaskIcon = parent.makeFunc(parent.modules.CCTaskToolkitSetTaskIcon, c_bool, [c_char_p])
			# Creates a parameter for the currently declared task
			# typeStr is the type of the parameter, its values can be:
			#   "Bool" (boolean value)
			#   "Float" (32 bits floating point)
			#   "Int" (32 bits integer)
			#   "String"
			#   "StringList" (a list of candidates to choose one from)
			#   "CheckList" (a list of candidates to choose one or several from)
			#   "VariableList" (a list of candidates to choose one from, but defined in a external variable)
			#   "VariableMultiSelectList" (a list of candidates to choose one or several from, but defined in a external variable)
			#   "Color (a hexcode color)
			#																												internalName   displayName	 description	 typeStr	 isRequired
			self.CreateParameterFromString = parent.makeFunc(parent.modules.CCTaskToolkitCreateParameterFromString, c_bool, [c_char_p, c_char_p, c_char_p, c_char_p, c_bool])
			# Creates a parameter for the currently declared task
			# same as above but uses an enumeration to define the type, use the CCTaskToolkitParameterDataType class for the enum
			self.CreateParameter = parent.makeFunc(parent.modules.CCTaskToolkitCreateParameter, c_bool, [c_char_p, c_char_p, c_char_p, c_int32, c_bool])
			# set the default value of the currently declared parameter, note you need to stringify it in case it is not a string
			self.SetParameterDefaultValue = parent.makeFunc(parent.modules.CCTaskToolkitSetParameterDefaultValue, c_bool, [c_char_p])
			# Can only be used for StringList and CheckList
			# Sets the candidates of the currently declared parameter, use comma separated values ( "value1,value2,value3" )
			self.SetParameterCandidates = parent.makeFunc(parent.modules.CCTaskToolkitSetParameterCandidates, c_bool, [c_char_p])
			# Can only be used for VariableList and VariableMultiSelectList
			# Specify the internalName of the variable containing the list of candidates
			self.SetParameterCandidatesSourceVariableInternalName = parent.makeFunc(parent.modules.CCTaskToolkitSetParameterCandidatesSourceVariableInternalName, c_bool, [c_char_p])
			# Creates a new Variable. If the variable is set to be a user variable, it will be editable by anyone, otherwise it can only be modified by the application publishing the catalog
			# variableType values can be found in the CCTaskToolkitVariableDataType class
			# integer : 0
			# float : 1
			# bool : 2
			# enum : 3
			# Json : 4
			# String : 5
			# Dictionnary : 6
			#																					   variableName	variableType	isUserVariable
			self.BeginVariable = parent.makeFunc(parent.modules.CCTaskToolkitBeginVariable, c_bool, [c_char_p, c_int32, c_bool])
			# TODO: is it the closing function of BeginVariable ?
			self.EndDatastoreVariable = parent.makeFunc(parent.modules.CCTaskToolkitEndDatastoreVariable, c_bool, [c_bool])
			# Can only be used on Bool Variable, sets its value
			self.SetDatastoreBoolVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreBoolVariableValue, c_bool, [c_bool])
			# Can only be used on Integer Variable, sets its value
			self.SetDatastoreIntVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreIntVariableValue, c_bool, [c_int32])
			# Can only be used on Float Variable, sets its value
			self.SetDatastoreFloatVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreFloatVariableValue, c_bool, [c_double])
			# Can only be used on String Variable, sets its value
			self.SetDatastoreStringVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreStringVariableValue, c_bool, [c_char_p])
			# Can only be used on Json Variable, sets its value (stringified json)
			self.SetDatastoreJSONVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreJSONVariableValue, c_bool, [c_char_p])
			# Can only be used on Dictionnary Variable, sets its value (0 based index)
			self.SetDatastoreEnumVariableValue = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreEnumVariableValue, c_bool, [c_int32])
			# Can only be used on Dictionnary Variable, associate a name and a color to a dictionnary index, can be called multiple times
			#																															name	   index   graphanaColor (red,green,blue,etc...)
			self.AddDatastoreEnumVariableMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreEnumVariableMapping, c_bool, [c_char_p, c_int32, c_char_p])
			# Deprecated: use AddDatastoreDictionnaryGenericMapping or AddDatastoreDictionnaryIntMapping instead.
			# Can only be used on Dictionnary Variable, creates a key:int value pair in the dictionnary. 
			#																										key		value
			self.AddDictionnaryMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDictionnaryMapping, c_bool, [c_char_p, c_int32])
			# Can only be used on Dictionnary Variable, creates a key:value pair in the dictionnary. The value can be anything and should be converted to string representation. We need to report its type (raw enum value from CCTaskToolkitVariableDataType).
			#																																			key		type	value
			self.AddDatastoreDictionnaryGenericMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryGenericMapping, c_bool, [c_char_p, c_int32, c_char_p])
			# Can only be used on Dictionnary Variable, creates a key:int value pair in the dictionnary
			#																																	key		value
			self.AddDatastoreDictionnaryIntMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryIntMapping, c_bool, [c_char_p, c_int32])
			# Can only be used on Dictionnary Variable, creates a key:boolean value pair in the dictionnary
			#																																	key		value
			self.AddDatastoreDictionnaryBoolMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryBoolMapping, c_bool, [c_char_p, c_bool])
			# Can only be used on Dictionnary Variable, creates a key:float value pair in the dictionnary
			#																																		key		value
			self.AddDatastoreDictionnaryFloatMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryFloatMapping, c_bool, [c_char_p, c_double])
			# Can only be used on Dictionnary Variable, creates a key:value pair in the dictionnary.  The value is a string or enum identifier. It it is a json, use AddDatastoreDictionnaryJSONMapping instead.
			#																																		 key	  value
			self.AddDatastoreDictionnaryStringMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryStringMapping, c_bool, [c_char_p, c_int32])
			# Can only be used on Dictionnary Variable, creates a key:json value pair in the dictionary.
			#																																	 key		json
			self.AddDatastoreDictionnaryJSONMapping = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreDictionnaryJSONMapping, c_bool, [c_char_p, c_char_p])
			# TODO
			self.SetDatastoreVariableUpdateValueInDatabase = parent.makeFunc(parent.modules.CCTaskToolkitSetDatastoreVariableUpdateValueInDatabase, c_bool, [c_bool])
			# add a tag to the currently declared variable, can be called multiple times
			self.AddDatastoreVariableTag = parent.makeFunc(parent.modules.CCTaskToolkitAddDatastoreVariableTag, c_bool, [c_char_p])
			# returns the number of errors of the last PublishCatalog call
			self.PublisherErrorCount = parent.makeFunc(parent.modules.CCTaskToolkitPublisherErrorCount, c_uint64, [])
			# TODO returns the currentError (top of the stack)
			self.PublisherGetCurrentError = parent.makeFunc(parent.modules.CCTaskToolkitPublisherGetCurrentError, c_char_p, [])
			# TODO move the currentError deeper in the stack
			self.PublisherNextError = parent.makeFunc(parent.modules.CCTaskToolkitPublisherNextError, c_bool, [])
			# clear the error stack
			self.PublisherClearErrors = parent.makeFunc(parent.modules.CCTaskToolkitPublisherClearErrors, None, [])
			# TODO
			self.PublisherClear = parent.makeFunc(parent.modules.CCTaskToolkitPublisherClear, None, [])
			# Publishes the catalog, make sure to not execute this while constructing the catalog (i.e. between a call to BeginCatalog and EndCatalog)
			self.PublishCatalog = parent.makeFunc(parent.modules.CCTaskToolkitPublishCatalog, c_bool, [c_char_p])


	class Utility:
		def __init__(self, parent):
			self.ParameterIsOfType = parent.makeFunc(parent.modules.CCTaskToolkitParameterIsOfType, c_int32, [c_char_p, POINTER(c_int32), c_uint64])
			# Try to convert typeStr (case unsensitive) into CCTaskToolkitParameterDataType enumeration (you can cast the returned integer).
			# Returns CCTaskToolkitParameterDataType.Count value if parsing unsuccessful.
			#																								   paramTypeStr ( "Integer" or "Float", etc)
			self.ParseParameterType = parent.makeFunc(parent.modules.CCTaskToolkitParseParameterType, c_int32, [c_char_p])
			#  Produce a version integer from a major, minor, patch integers, and that can be compared with simple <=> operators with GetVersion tasktoolkit function.
			#																					   major	   minor	   patch
			self.MakeVersion = parent.makeFunc(parent.modules.CCTaskToolkitMakeVersion, c_uint64, [c_uint16, c_uint16, c_uint16])
			# Returns the current version as u64 for easy comparisons.
			# 1st-2nd bytes are the major version, 3rd-4th bytes are the minor, 5th-6th bytes the subminor and 7th-8th is reserved.
			self.GetVersion = parent.makeFunc(parent.modules.CCTaskToolkitGetVersion, c_uint64, [])
			# Returns the current version as a string "major.minor.patch"
			self.GetVersionString = parent.makeFunc(parent.modules.CCTaskToolkitGetVersionString, c_char_p, [])

		def getToolkitVersion(self):
			return self.GetVersionString().decode('utf-8')


	class Pulse:
		def __init__(self, parent):
			# Setup pulseID from mf 3 letters project code, zone, appname (capsuleOrGoal) and a instance number.
			#																						   projectCode	 zoneName		capsuleName	 instanceNb
			self.SetupPulseID = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetupPulseID, c_bool, [c_char_p, c_char_p, c_char_p, c_char_p])
			# Optional: Control the pulse version to use in case we don't want the latest (default).
			self.SetAPIVersion = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetAPIVersion, c_bool, [c_int32])
			# Allow to create internal heartbeat and post them through CCTaskToolkitPulseOnPostStartupCallback and CCTaskToolkitPulseOnPostHeartbeatCallback, even if we're not connected to CC.
			self.SetPulseOfflineEnabled = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetEnabledOffline, c_bool, [c_bool])
			# Allow Tasktoolkit to auto-compute application's windows metrics (resolution, focus state, maximize state, ...). This is not in the specs so this setting is disabled by default.
			self.SetPulseWindowMetricsEnabled = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetPulseWindowMetricsEnabled, c_bool, [c_bool])
			# Setup the pulse connection
			#  mode is to choose what protocol to use, but it is unused for now since mqtt is the only one implemented.
			#  address is the address of the mqtt broker in case we choose mqtt as our protocol.
			#																												  serverIP (by default mqttbrokerIp)		protocol (for now only 0 for mqtt)
			self.SetupConnectionSettings = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetupConnectionSettings, c_bool, [c_char_p, c_int32])
			# Set username and password needed to connect to pulse mqtt broker.
			self.SetupConnectionLogin = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetupConnectionLogin, c_bool, [c_char_p, c_char_p])
			# Set the heartbeat and pulse thread refresh time steps (in fractional seconds).
			# TODO: should have a getter in case the interval is changed by the API ? Or have a callback called when the heartbeat info is required ?
			# Set a value <= 0.0 to leave it unchanged.
			#  - heartbeatTimeStepSeconds is the time step we'll pushed heartbeat.
			#  - pulseThreadRefreshTimeStepSeconds is the time step of the pulse thread. Lower value means higher load, but less lag in processing pulse requests coming from outside.
			#	It cannot be less than heartbeatTimeStepSeconds. In case it happens, it would be set automatically to heartbeatTimeStepSeconds value.
			#																										   heartbeatTimeStepSeconds	pulseThreadRefreshTimeStepSeconds
			self.SetHeartbeatInterval = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetHeartbeatInterval, c_bool, [c_double, c_double])
			# if set to True (default) heartbeat payloads will only be sent if API update function is called regularly
			self.WatchAPIUpdate = parent.makeFunc(parent.modules.CCTaskToolkitPulseWatchAPIUpdate, c_bool, [c_int64])
			# Manually provide an application exit reason Pulse API cannot compute automatically.
			# If sendExit is true, then Pulse will immediately send an exit message and cleanup itself.
			# specify false if you want to wait for the signal to wait for the call to cleanUpCCTaskToolkitAPI or cleanUpAPI to send the message.
			#																								   exitCause   sendExit
			self.NotifyExitCause = parent.makeFunc(parent.modules.CCTaskToolkitPulseNotifyExitCause, c_bool, [c_char_p, c_bool])
			# Allow some exception monitoring to find out the exit cause if no exit cause was provided. By default, none are enabled.
			# That method should be called once or never. Undefined behavior if it is called more.
			# - allowSignalMonitoring : true to install signal handlers. Note that we still forward to the previous signal handler.
			# - allowAtExitHandlers : true if we should try to cleanup the Pulse API (and send the exit notification in the process) when at exit handlers are called.
			#																														 allowSignalMonitoring  allowAtExitHandlers
			self.AllowSetupExceptionHandlers = parent.makeFunc(parent.modules.CCTaskToolkitPulseAllowSetupExceptionHandlers, c_bool, [c_bool, c_bool])
			# Declare a module with its version is used by the calling application.
			# - category: the category of the module, see  PulseVersionCategory class for the values
			# - moduleName: the name of the module
			# - moduleVersion: the version of the module
			#																										   category	moduleName  moduleVersion
			self.DeclareVersionModule = parent.makeFunc(parent.modules.CCTaskToolkitPulseDeclareVersionModule, c_bool, [c_int32, c_char_p, c_char_p])
			# Event function that should be called each frame to set :
			# - the average fps. Don't compute it yourself. Either provide the application SDK computed framerate, or pass NaN (Jukebox will compute it for you efficiently)
			# - frameDrop: how much frame were dropped since the application start.
			# Note: Calling that method is cheap since any processing is done on another thread.
			#																								averageFps	 framesDropped
			self.NotifyNewFrame = parent.makeFunc(parent.modules.CCTaskToolkitPulseNotifyNewFrame, c_bool, [c_double, c_uint64])

			# Same as NotifyNewFrame but it allows to manually set what would be the total frame instead of leaving Jukebox api computes it automatically by incrementing its internal counter.
			#																								averageFps	 framesDropped	 totalFrames
			self.NotifyNewFrameWithTotalFrame = parent.makeFunc(parent.modules.CCTaskToolkitPulseNotifyNewFrameWithTotalFrame, c_bool, [c_double, c_uint64, c_uint64])
			# Create a new metrics in pulse. This returns a pusher handle, that should be stored because it is used to reference this metrics pusher.
			# TODO does the timeout logic applies to all categories ? Should create three pushers for metric, state, and subsystem ?
			# This pusher would keep the data value until timeoutSec expires or a new call to the set method happened. If this value is negative, the data won't expire.
			# Note : A pusher can hold more than one data value.
			# - name: name of the metric, state, or role
			# - category. use the PulseMetricsCategory class to choose the right value
			#																														name		caterogy	   timeoutSeconds
			self.AddNewCustomMetricsPusher = parent.makeFunc(parent.modules.CCTaskToolkitPulseAddNewCustomMetricsPusher, c_void_p, [c_char_p, c_int32, c_int32])
			# Set a new maximum stack size between 4 and 65535 to the external metrics. Default is 20.
			# Call this method only if you ensured you called as many begin object/table as endStack but still hit the maximum nested object/table stack.
			#																															   pusherHandle	newMaxStackSize
			self.SetExternalMetricsMaxStackSize = parent.makeFunc(parent.modules.CCTaskToolkitPulseSetExternalMetricsMaxStackSize, c_bool, [c_void_p, c_uint16])
			# Set a 64-bit int value at keyName key or create a new one into the pusher specified by pusherHandle.
			# Note : keyname is the name that would appear in the json.
			# Note 2 : The handle should be valid or null.
			self.ExternalMetricsSetIntValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetIntValue, c_bool, [c_void_p, c_char_p, c_int64])
			# Set a 64-bit unsigned int value at keyName key or create a new one into the pusher specified by pusherHandle.
			# Note : keyname is the name that would appear in the json.
			# Note 2 : The handle should be valid or null.
			#																														   pusherHandle	key			 value
			self.ExternalMetricsSetUIntValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetUIntValue, c_bool, [c_void_p, c_char_p, c_uint64])
			# Same comments apply to the value functions below
			self.ExternalMetricsSetDoubleValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetDoubleValue, c_bool, [c_void_p, c_char_p, c_double])
			self.ExternalMetricsSetBoolValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetBoolValue, c_bool, [c_void_p, c_char_p, c_bool])
			self.ExternalMetricsSetStringValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetStringValue, c_bool, [c_void_p, c_char_p, c_char_p])
			self.ExternalMetricsSetJSONValue = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsSetJSONValue, c_bool, [c_void_p, c_char_p, c_char_p])
			# Begin an object declaration inside the pusher specified by pusherHandle.
			# A call to [CCTaskToolkitPulse]ExternalMetricsEndStack method should match every call to this method.
			# TODO: how to populate the object with ? Oher values ?
			#																													   pusherHandle
			self.ExternalMetricsBeginObject = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsBeginObject, c_bool, [c_void_p, c_char_p])
			# Begin an table declaration inside the pusher specified by pusherHandle.
			# A call to [CCTaskToolkitPulse]ExternalMetricsEndStack method should match every call to this method.
			# TODO: how to populate the table with ? Oher values ?
			self.ExternalMetricsBeginTable = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsBeginTable, c_bool, [c_void_p, c_char_p])
			# End an object or table declaration inside the pusher specified by pusherHandle and we'll go back to the parent(previous unclosed) object/table declaration.
			# The call to this method must match any call to object/table begin method.
			self.ExternalMetricsEndStack = parent.makeFunc(parent.modules.CCTaskToolkitPulseExternalMetricsEndStack, c_bool, [c_void_p])
			# Remove the metrics specified by pusherHandleToRemove.
			# After the call, the handle becomes invalid and it's up to the client code to set it to NULL/nullptr/IntPtr.Zero.
			self.RemoveExternalMetrics = parent.makeFunc(parent.modules.CCTaskToolkitPulseRemoveExternalMetrics, c_bool, [c_void_p])

			self.heartbeat = ToolkitProvider.Pulse.HeartBeat(self)
			self.startup = ToolkitProvider.Pulse.Startup(self)
			self.exit = ToolkitProvider.Pulse.Exit(self)

		# set the heartbeat refresh interval
		def setHeartBeatInterval(self, intervalSeconds):
			self.SetHeartbeatInterval(float(intervalSeconds), float(intervalSeconds))

		# helper functions
		def _setPusherKeyValue(self, pusher, key, value):
			if isinstance(value, bool):
				self.ExternalMetricsSetBoolValue(pusher, key.encode('utf-8'), value)
			elif isinstance(value, str):
				self.ExternalMetricsSetStringValue(pusher, key.encode('utf-8'), value.encode('utf-8'))
			elif isinstance(value, int):
				self.ExternalMetricsSetIntValue(pusher, key.encode('utf-8'), value)
			elif isinstance(value, float):
				self.ExternalMetricsSetDoubleValue(pusher, key.encode('utf-8'), value)
			elif isinstance(value, dict):
				self.ExternalMetricsBeginObject(pusher, key.encode('utf-8'))
				for dictKey, dictValue in value.items():
					self._setPusherKeyValue(pusher, dictKey, dictValue)
				self.ExternalMetricsEndStack(pusher)

		class Startup:
			def __init__(self, parent):
				self.parent = parent

			def addRuntimeVersion(self, runtimeName, runtimeVersion):
				self.parent.DeclareVersionModule(PulseVersionCategory.Runtime, runtimeName.encode('utf-8'), runtimeVersion.encode('utf-8'))

			def addModuleVersion(self, moduleName, moduleVersion):
				self.parent.DeclareVersionModule(PulseVersionCategory.Component, moduleName.encode('utf-8'), moduleVersion.encode('utf-8'))

			# TODO to fill up by future tasktoolkit method to return the startup payload json dictionnary
			def getNotificationPayload(self):
				return

		class Exit:
			def __init__(self, parent):
				self.parent = parent

			def notifyExit(self, exitCause):
				self.parent.NotifyExitCause(exitCause.encode('utf-8'), True)

			# TODO to fill up by future tasktoolkit method to return the exit payload json dictionnary
			def getNotificationPayload(self, exitCause):
				return

		class HeartBeat:
			def __init__(self, parent):
				self.__customPushers = [dict(), dict(), dict(), dict()]
				self.fps = None
				self.framedrops = None
				self.parent = parent

			def __removePusherAtKey(self, name, category):
				pushers = self.__customPushers[category]
				if name in pushers:
					existingPusher = pushers[name]
					self.parent.RemoveExternalMetrics(existingPusher)
					pushers.pop(name)

			def onNewFrame(self, averageFps, droppedFrames):
				# this should not be called if initializeAPI has not been called yet
				self.parent.NotifyNewFrame(c_double(averageFps), c_uint64(droppedFrames))

			def getOrCreatePusher(self, name, category):
				if name not in self.__customPushers[category]:
					self.__customPushers[category][name] = self.parent.AddNewCustomMetricsPusher(name.encode('utf-8'), category, -1)
				return self.__customPushers[category][name]

			def setCustomState(self, stateName, stateValue):
				try:
					json.dumps(stateName)
					json.dumps(stateValue)
				except (TypeError, OverflowError):
					print("setCustomState Error: please use parameters that can be serializable in json")
					return False
				pusher = self.getOrCreatePusher(stateName, PulseMetricsCategory.State)
				self.parent._setPusherKeyValue(pusher, stateName, stateValue)
				return True

			def removeCustomState(self, stateName):
					self.__removePusherAtKey(stateName, PulseMetricsCategory.State)

			def setCustomRole(self, roleName, roleValue):
				try:
					json.dumps(roleName)
					json.dumps(roleValue)
				except (TypeError, OverflowError):
					print("setCustomRole Error: please use parameters that can be serializable in json")
					return False
				pusher = self.getOrCreatePusher(roleName, PulseMetricsCategory.Role)
				self.parent._setPusherKeyValue(pusher, roleName, roleValue)
				return True

			def removeCustomRole(self, roleName):
				self.__removePusherAtKey(roleName, PulseMetricsCategory.Role)

			def setCustomMetric(self, metricName, metricValue):
				try:
					json.dumps(metricName)
					# check if metricValue is a number by trying to round it
					round(metricValue,1)
				except (TypeError, OverflowError):
					print("setCustomMetric Error: metricName should be a string and metricValue a number")
					return False
				pusher = self.getOrCreatePusher(metricName, PulseMetricsCategory.Metrics)
				self.parent._setPusherKeyValue(pusher, metricName, metricValue)
				return True

			def removeCustomMetric(self, metricName):
				self.__removePusherAtKey(metricName, PulseMetricsCategory.Metrics)

			def setCustomSubsystem(self, subsystemName, subsystemStatusDict):
				if isinstance(subsystemStatusDict, dict):
					json.dumps(subsystemName)
					if not "status" in subsystemStatusDict.keys():
						print("setCustomSubsystem Error: subsystemStatusDict must contain the 'status' key")
						return False
				else:
					print("setCustomSubsystem Error: subsystemName should be a string and subsystemStatusDict a dictionnary")
					return False
				self.parent._setPusherKeyValue(self.getOrCreatePusher(subsystemName, PulseMetricsCategory.Subsystem), subsystemName, subsystemStatusDict)
				return True

			def removeCustomSubsystem(self, subsystemName):
				self.__removePusherAtKey(subsystemName, PulseMetricsCategory.Subsystem)

			# TODO to fill up by future tasktoolkit method to return the exit payload json dictionnary
			def getNotificationPayload(self, exitCause):
				return

	class Calibration:
		def __init__(self, parent):
			# Set the calibration technique to use selected from to CalibrationTechnique enum.
			self.SetCalibrationTechnique = parent.makeFunc(parent.modules.CCTaskToolkitSetCalibrationTechnique, c_bool, [c_int32])
			# Clear all registered calibration points.
			self.Clear = parent.makeFunc(parent.modules.CCTaskToolkitClearCalibration, c_bool, [])
			# Set the source (reference) device ID and the one being calibrated from the source.
			self.SetDevicesID = parent.makeFunc(parent.modules.CCTaskToolkitCalibrationSetDevicesID, c_bool, [c_uint32, c_uint32])
			# Add 2 triplets of the same position coordinates expressed in the reference device frame and the calibrated device frame.
			self.AddCalibrationPoints = parent.makeFunc(parent.modules.CCTaskToolkitAddCalibrationPoints, c_bool, [c_double, c_double, c_double, c_double, c_double, c_double])
			# Run the calibration with registered points
			self.Execute = parent.makeFunc(parent.modules.CCTaskToolkitCalibrationExecute, c_bool, [])
