import pulse 
import time

# call this when your application has finished starting and is operational
pulseStartup = pulse.Startup(projectName="pro", zoneName="zone", appName="myApp", techName="tech", mqttBrokerHostAndPort="127.0.0.1:1883")
pulseStartup.addModuleVersion(moduleName="module1", moduleVersion="1.0.1")
pulseStartup.addRuntimeVersion(runtimeName='runtime1', runtimeVersion="0.0.1")
pulseHeartbeat = pulse.HeartBeat()

# beware that this is called from pulse own thread, so this has to be thread safe :)
totalFrame = 0
def onHeartbeatRequest():
    global totalFrame
    pulseHeartbeat.setFrameInfo(averageFps=30, droppedFrames=0, totalFrames=totalFrame)

    # provide information on metrics, state, role
    pulseHeartbeat.setCustomMetric(metricName="intMetric", metricValue= 20)
    pulseHeartbeat.setCustomMetric(metricName="floatMetric", metricValue= 1.234)
    pulseHeartbeat.setCustomRole(roleName="roleName", roleValue="roleValue")

    pulseHeartbeat.setCustomState(stateName="intState", stateValue=1)
    pulseHeartbeat.setCustomState(stateName="floatState", stateValue=1.1234)
    pulseHeartbeat.setCustomState(stateName="stringState", stateValue="stateStringValue")
    pulseHeartbeat.setCustomState(stateName="jsonState", stateValue={ "intProp" : 1, "stringProp" : "toto", "boolProp" : True })

    pulseHeartbeat.setCustomSubsystem(subsystemName="subsystem1", subsystemStatusDict={"status": "ok", "floatProp": 1.89})
    pulseHeartbeat.setCustomSubsystem(subsystemName="subsystem2", subsystemStatusDict={"status": "overheat", "tempCelcius": 89})

    totalFrame += 1
    
# register the callback to be notified a new heartbeat is required
pulse.setHeartBeatRequestCallback(onHeartbeatRequest)

# Once the application exit
pulse.Exit("Loop Finished")
