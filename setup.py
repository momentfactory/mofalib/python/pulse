from setuptools import find_packages, setup
import os
version = '3.8.3'

setup(
    name='jukebox',
    packages=find_packages(include=['jukebox']),
    package_data={'': ['*.dll','*.crt']},
    version = version,
    description='A python library to implement jukebox api',
    author='Moment Factory',
    license='MIT'
)